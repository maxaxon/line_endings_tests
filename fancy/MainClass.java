package ch.axonivy.fintech.acrevis.component;

import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import org.apache.commons.collections4.CollectionUtils;

import ch.axonivy.fintech.acrevis.component.AddressComponent.AddressComponentData;
import ch.axonivy.fintech.basic.component.utility.CountryUtility;
import ch.axonivy.fintech.standard.core.locale.CountryDto;
import ch.axonivy.fintech.standard.guiframework.core.BaseGuiFrameworkController;
import ch.axonivy.fintech.standard.guiframework.core.GuiFrameworkControllerConfig;
import ch.axonivy.fintech.standard.guiframework.exception.GuiFrameworkException;
import ch.axonivy.fintech.standard.guiframework.util.AutocompleteUtil;
import ch.axonivy.fintech.standard.guiframework.util.GuiFrameworkUtil;
import ch.ivyteam.ivy.environment.Ivy;

public class AddressComponentController extends BaseGuiFrameworkController<AddressComponentData> {

	private static final String ATTR_COUNTRIES = "_countries";

	AddressComponentController(GuiFrameworkControllerConfig<AddressComponentData> config) {
		super(config);
	}

	public static AddressComponentController createInstance(GuiFrameworkControllerConfig<AddressComponentData> config) {
		return new AddressComponentController(config);
	}


	@Override
	public void doPostAddToViewEvent(ComponentSystemEvent event, AddressComponentData viewModel) throws GuiFrameworkException {
		super.doPostAddToViewEvent(event, viewModel);
		if(CollectionUtils.isEmpty(viewModel.getCountries())){
			List<CountryDto> countrylist = CountryUtility.getSortedByNameCountries(Ivy.session().getContentLocale());
			viewModel.setCountries(countrylist);
		}
		String key = GuiFrameworkUtil.getAutocompleteBean().getEffectiveComponentClass(viewModel);
		String countriesKey = key + ATTR_COUNTRIES;
		AutocompleteUtil.initAutocompleteSource(viewModel.getCountries(), countriesKey); 
	}

}
