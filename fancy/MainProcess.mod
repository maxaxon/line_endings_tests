[Ivy]
16229183E0AFE429 3.20 #module
>Proto >Proto Collection #zClass
ck0 createRestartArchivingTask Big #zClass
ck0 B #cInfo
ck0 #process
ck0 @TextInP .resExport .resExport #zField
ck0 @TextInP .type .type #zField
ck0 @TextInP .processKind .processKind #zField
ck0 @AnnotationInP-0n ai ai #zField
ck0 @MessageFlowInP-0n messageIn messageIn #zField
ck0 @MessageFlowOutP-0n messageOut messageOut #zField
ck0 @TextInP .xml .xml #zField
ck0 @TextInP .responsibility .responsibility #zField
ck0 @PushWFArc f6 '' #zField
ck0 @UserTask f0 '' #zField
ck0 @TkArc f2 '' #zField
ck0 @SignalStartEvent f3 '' #zField
ck0 @EndTask f1 '' #zField
ck0 @GridStep f5 '' #zField
ck0 @CallSub f13 '' #zField
ck0 @PushWFArc f7 '' #zField
ck0 @PushWFArc f4 '' #zField
ck0 @InfoButton f8 '' #zField
ck0 @AnnotationArc f9 '' #zField
>Proto ck0 ck0 createRestartArchivingTask #zField
ck0 f6 215 64 400 64 #arcP
ck0 f0 richDialogId ch.axonivy.fintech.standard.common.PassthroughDialog #txt
ck0 f0 startMethod start() #txt
ck0 f0 requestActionDecl '<> param;' #txt
ck0 f0 responseActionDecl 'ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData out;
' #txt
ck0 f0 responseMappingAction 'out=in;
' #txt
ck0 f0 outLinks "TaskA.ivp" #txt
ck0 f0 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=<%\=in.taskName%>
TaskA.PRI=2
TaskA.ROL=MA-Backoffice
TaskA.SKIP_TASK_LIST=false
TaskA.TYPE=0
TaskA.customFields.decimal.2=ch.axonivy.fintech.acrevis.task.TaskType.ELCA_FAILED.getUniqueCode()
TaskA.customFields.varchar.3=in.processParam.cobId
TaskA.customFields.varchar.4=ch.axonivy.fintech.acrevis.dossier.AcrevisDossierService.getInstance().findId(in.processParam.cobId)
TaskA.customFields.varchar.5=ch.axonivy.fintech.standard.core.enums.ProcessContext.ACTIVATION.name()' #txt
ck0 f0 type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
ck0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Restart Archiving ELCA</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ck0 f0 592 42 144 44 -63 -8 #rect
ck0 f0 @|UserTaskIcon #fIcon
ck0 f2 expr out #txt
ck0 f2 type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
ck0 f2 var in1 #txt
ck0 f2 512 64 592 64 #arcP
ck0 f3 actionDecl 'ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData out;
' #txt
ck0 f3 actionCode 'import ch.axonivy.fintech.standard.core.signal.simple.SignalCarriedData;
import ch.axonivy.fintech.acrevis.dossier.process.MainProcessParamVO.MainProcessParamVOBuilder;
import ch.axonivy.fintech.standard.core.signal.SignalDataConverter;


SignalCarriedData acrevisSignalData = SignalDataConverter.toSignalData(signal.getSignalData() as String, SignalCarriedData.class) as SignalCarriedData;
out.signalInfo.carriedData = acrevisSignalData;
out.processParam = MainProcessParamVOBuilder.createBuilder().ofDossier(acrevisSignalData.cobId).withContext(ch.axonivy.fintech.standard.core.enums.ProcessContext.ACTIVATION).setAction(ch.axonivy.fintech.standard.core.enums.ProcessAction.LOAD).build();' #txt
ck0 f3 type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
ck0 f3 signalCode ch_axonivy_fintech_acrevis:dossier:elcaCheck_ElcaFailed #txt
ck0 f3 attachToBusinessCase true #txt
ck0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ch_axonivy_fintech_acrevis:dossier:elcaCheck_ElcaFailed</name>
        <nameStyle>55,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ck0 f3 185 49 30 30 -159 17 #rect
ck0 f3 @|SignalStartEventIcon #fIcon
ck0 f1 type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
ck0 f1 969 49 30 30 0 15 #rect
ck0 f1 @|EndIcon #fIcon
ck0 f5 actionDecl 'ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData out;
' #txt
ck0 f5 actionTable 'out=in;
' #txt
ck0 f5 actionCode 'import ch.axonivy.fintech.acrevis.helper.TaskNameHelper;
import ch.axonivy.fintech.acrevis.helper.SignalHelper;
import ch.axonivy.fintech.acrevis.AcrevisDossier;
import ch.axonivy.fintech.acrevis.dossier.AcrevisDossierService;
import ch.axonivy.fintech.acrevis.datamodel.ProgressInfo;
import ch.axonivy.fintech.standard.dossier.ProgressInfo.ProgressInfoBuilder;
import ch.axonivy.fintech.portal.portalcustomization.task.TaskName;


AcrevisDossierService service = AcrevisDossierService.getInstance();
in.dossier = service.findByCobId(in.processParam.cobId) as AcrevisDossier;

in.responsibleRoleCodes = in.dossier.getProgressInfo().responsibleRoleCodes;

in.dossier.progressInfo = ProgressInfoBuilder.createBuilder()
	.inContext(in.processParam.context, in.processParam.genericInformationProcess)
	.detail(TaskName.from("/ch/axonivy/fintech/acrevis/TaskInformation/ElcaActivation/restartElcaArchiving", in.processParam.cobId).toJsonString())
	.responsibleRoleCodes(in.responsibleRoleCodes)
	.finished(false)
	.buildWith(in.dossier.progressInfo) as ProgressInfo;
in.dossier.progressInfo.firstApproval = SignalHelper.getApprovePerson(in.signalInfo);

service.update(in.dossier);
in.taskName = TaskNameHelper.buildTaskRestartELCAArchiving(in.dossier);' #txt
ck0 f5 type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
ck0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ck0 f5 400 42 112 44 -8 -8 #rect
ck0 f5 @|StepIcon #fIcon
ck0 f13 type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
ck0 f13 processCall 'Business Processes/MainStepProcesses/DocumentProcess:processDataGathering(ch.axonivy.fintech.acrevis.dossier.process.MainProcessParamVO)' #txt
ck0 f13 doCall true #txt
ck0 f13 requestActionDecl '<ch.axonivy.fintech.acrevis.dossier.process.MainProcessParamVO processParam> param;
' #txt
ck0 f13 requestMappingAction 'param.processParam=in.processParam;
' #txt
ck0 f13 responseActionDecl 'ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData out;
' #txt
ck0 f13 responseMappingAction 'out=in;
' #txt
ck0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>DocumentProcess</name>
        <nameStyle>15,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ck0 f13 800 42 112 44 -51 -8 #rect
ck0 f13 @|CallSubIcon #fIcon
ck0 f7 expr data #txt
ck0 f7 outCond ivp=="TaskA.ivp" #txt
ck0 f7 736 64 800 64 #arcP
ck0 f4 expr out #txt
ck0 f4 912 64 969 64 #arcP
ck0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Task #301
This task will be created when system cannot call ELCA in a limit times</name>
        <nameStyle>81,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ck0 f8 440 138 400 44 -193 -16 #rect
ck0 f8 @|IBIcon #fIcon
ck0 f9 640 138 613 86 #arcP
>Proto ck0 .type ch.axonivy.fintech.acrevis.process.AcrevisStartProcessData #txt
>Proto ck0 .processKind NORMAL #txt
>Proto ck0 0 0 32 24 18 0 #rect
>Proto ck0 @|BIcon #fIcon
ck0 f3 mainOut f6 tail #connect
ck0 f6 head f5 mainIn #connect
ck0 f5 mainOut f2 tail #connect
ck0 f2 head f0 in #connect
ck0 f0 out f7 tail #connect
ck0 f7 head f13 mainIn #connect
ck0 f13 mainOut f4 tail #connect
ck0 f4 head f1 mainIn #connect
ck0 f8 ao f9 tail #connect
ck0 f9 head f0 @CG|ai #connect  
